import { JoiValidationPipe,ValidationPipe } from './validation.pipe';
import { TestDto } from './test.dto';
import * as Joi from '@hapi/joi';
import {
  Controller,
  Get,
  ParseIntPipe,
  Param,
  HttpStatus,
  Body,
  Post,
  UsePipes,
} from '@nestjs/common';
import { AppService } from './app.service';

const testDataSchema = Joi.object({ // schema created for joi validation
  name: Joi.string().required(),
  id: Joi.number().optional(),
});

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post()
  @UsePipes(new JoiValidationPipe(testDataSchema))
  async create(@Body(new ValidationPipe()) testData: TestDto): Promise<TestDto> { // this one is called parameter scoped
    await this.appService.create(testData);
    console.log(testData);
    return testData;
  }

  @Get(':id')
  //  async getOne(@Param('id',ParseIntPipe) usrId:number):Promise<any>{  HERE WE HAVE PASSED DIRECT CLASS AND LET NEST DO THE DEPENDENCY INJECTION N ALL
   async getOne(@Param('id',new ParseIntPipe()) usrId:number):Promise<any>{ // custom pipe transform implemented
  // async getOne(
  //   @Param(
  //     'id',
  //     new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
  //   )
  //   usrId: number,
  // ): Promise<any> {
    return this.appService.gettingOne(usrId);
  }
}
// check main.ts