import {PipeTransform,Injectable,ArgumentMetadata, BadRequestException} from '@nestjs/common';
import { ObjectSchema } from '@hapi/joi';
import { validate } from 'class-validator';
import {plainToClass} from 'class-transformer';

@Injectable()

export class JoiValidationPipe implements PipeTransform {

// transform(value:number,metadata:ArgumentMetadata):any{
//     console.log(metadata);
//     return value; // just a basic validation pipe which takes an input and return that input
// }

constructor(private schema:ObjectSchema){}

transform(value:any,metaData:ArgumentMetadata):any{

const {error} = this.schema.validate(value);

if(error){
    throw new BadRequestException('validation failed');
}
return value;
}


}



@Injectable()
export class ValidationPipe implements PipeTransform<any>{

async transform(value:any,{metatype}:ArgumentMetadata){
    if(!metatype||!this.toValidate(metatype)){
        return value;
    }

const object = plainToClass(metatype,value);
const errors = await validate(object);
if(errors.length> 0){
    throw new BadRequestException('Validation failed sorry');
}
return value;
}
// eslint-disable-next-line @typescript-eslint/ban-types
private toValidate(metatype:Function):boolean{
    // eslint-disable-next-line @typescript-eslint/ban-types
    const types:Function[] = [String,Boolean,Number,Array,Object];
    return !types.includes(metatype);
}

}

