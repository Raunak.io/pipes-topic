import { IsString, IsNumber } from 'class-validator';

export class TestDto {
  @IsString()
  name: string;

  @IsNumber()  // decorator based validators by nestjs 
  id: number;
}
