import { TestDto } from './test.dto';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
private dataset:TestDto[]=[]

  getHello(): string {
    return 'Hello World!';
  }

  async gettingOne(id: number): Promise<any> {
    return `${id}: is the user id`;
  }

async create(data:TestDto):Promise<string>{

   this.dataset.push(data);
    return  'created successfully';
}


}
